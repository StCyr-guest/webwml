<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>We recommend that you upgrade your clamav packages.</p>

<p>Upstream published version 0.99.2.  This update updates wheezy-lts to the
latest upstream release in line with the approach used for other Debian
releases.</p>

<p>The changes are not strictly required for operation, but users of the previous
version in Wheezy may not be able to make use of all current virus signatures
and might get warnings.</p>

<p>For Debian 7 <q>Wheezy</q>, this has been addressed in version
0.99.2+dfsg-0+deb7u1.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

<p>For Debian 6 <q>Squeeze</q>, these issues have been fixed in clamav version 0.99.2+dfsg-0+deb7u1</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-546.data"
# $Id: $
