<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Paul Rohar discovered that libdbd-mysql-perl, the Perl DBI database
driver for MySQL and MariaDB, constructed an error message in a
fixed-length buffer, leading to a crash (_FORTIFY_SOURCE failure) and,
potentially, to denial of service.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.021-1+deb7u2.</p>

<p>We recommend that you upgrade your libdbd-mysql-perl packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-656.data"
# $Id: $
