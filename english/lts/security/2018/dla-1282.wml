<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A denial of service vulnerability has been discovered in graphicsmagick,
 a collection of image processing tools and associated libraries.</p>

<p>A specially crafted file can be used to produce a denial of service
(heap overwrite) or possible other unspecified impact by exploiting a
defect related to unused pixel staging area in the AcquireCacheNexus
function in magick/pixel_cache.c.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.3.16-1.1+deb7u18.</p>

<p>We recommend that you upgrade your graphicsmagick packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1282.data"
# $Id: $
