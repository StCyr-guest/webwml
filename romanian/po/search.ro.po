msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2016-09-20 11:49+0100\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: \n"
"Language: ro\n"
"X-Generator: Poedit 1.8.8\n"

#: ../../english/searchtmpl/search.def:6
msgid "Search for"
msgstr "Caută"

#: ../../english/searchtmpl/search.def:10
msgid "Results per page"
msgstr "Rezultate pe pagină"

#: ../../english/searchtmpl/search.def:14
msgid "Output format"
msgstr "Formatul rezultatelor"

#: ../../english/searchtmpl/search.def:18
msgid "Long"
msgstr "Lung"

#: ../../english/searchtmpl/search.def:22
msgid "Short"
msgstr "Scurt"

#: ../../english/searchtmpl/search.def:26
msgid "URL"
msgstr "Adrese"

#: ../../english/searchtmpl/search.def:30
msgid "Default query type"
msgstr "Tip de cerere predefinit&#259;"

#: ../../english/searchtmpl/search.def:34
msgid "All Words"
msgstr "Toate cuvintele"

#: ../../english/searchtmpl/search.def:38
msgid "Any Words"
msgstr "Cel pu&#355;in un cuv&#226;nt"

#: ../../english/searchtmpl/search.def:42
msgid "Search through"
msgstr "Caut&#259; printre"

#: ../../english/searchtmpl/search.def:46
msgid "Entire site"
msgstr "&#206;ntregul site"

#: ../../english/searchtmpl/search.def:50
msgid "Language"
msgstr "Limbă"

#: ../../english/searchtmpl/search.def:54
msgid "Any"
msgstr "Oricare"

#: ../../english/searchtmpl/search.def:58
msgid "Search results"
msgstr "Rezultatele căutării"

#: ../../english/searchtmpl/search.def:65
msgid ""
"Displaying documents \\$(first)-\\$(last) of total <B>\\$(total)</B> found."
msgstr ""
"Documentele \\$(first)-\\$(last) dintr-un total de <B>\\$(total)</B> "
"documente g&#259;site."

#: ../../english/searchtmpl/search.def:69
msgid ""
"Sorry, but search returned no results. <P>Some pages may be available in "
"English only, you may want to try searching again and set the Language to "
"Any."
msgstr ""
"Ne pare r&#259;u, dar c&#259;utarea nu a g&#259;sit nimic. <P>Unele pagini "
"pot fi disponibile numai &#238;n englez&#259;, poate dori&#355;i s&#259; "
"reface&#355;i c&#259;utarea &#351;i s&#259; seta&#355;i limba la oricare."

#: ../../english/searchtmpl/search.def:73
msgid "An error occured!"
msgstr "Eroare!"

#: ../../english/searchtmpl/search.def:77
msgid "You should give at least one word to search for."
msgstr "Căutarea necesită cel puțin un cuvant"

#: ../../english/searchtmpl/search.def:81
msgid "Powered by"
msgstr "Ac&#355;ionat de"

#: ../../english/searchtmpl/search.def:85
msgid "Next"
msgstr "Urm&#259;torul"

#: ../../english/searchtmpl/search.def:89
msgid "Prev"
msgstr "Precedentul"
