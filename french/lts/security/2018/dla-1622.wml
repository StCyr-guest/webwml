#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>debian-security-support, le contrôleur de couverture de prise en charge de
sécurité de Debian, a été mis à jour dans Jessie. Les modifications les plus
importantes pour Jessie sont :</p>

<ul>
<li>fin de vie pour jasperreports dans Jessie ;</li>
<li>abandon de la prise en charge de webkit2gtk dans toutes les publications
    (fermeture du bogue n°914567) ;</li>
<li>fin de vie pour jruby dans Jessie conformément à DSA-4219-1 (fermeture du
    bogue n°901032) ;</li>
<li>fin de vie de vlc dans Jessie conformément à DSA 4203-1 ;</li>
<li>abandon de la prise en charge de frontaccounting ;</li>
<li>fin de vie pour redmine pour Debian 8 (Jessie) (fermeture du
    bogue n°897609).</li>
</ul>

<p>Pour Debian 8 <q>Jessie</q>, la version du paquet est 2018.11.25~deb8u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets debian-security-support.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

<p>Pour Debian 6 <q>Squeeze</q>, ces problèmes ont été corrigés dans
la version  2018.11.25~deb8u2 de debian-security-support.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1622.data"
# $Id: $
